import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation
from scipy import signal

pShaping = np.ones((1, 30), dtype=int)
pShaping[0, 15:] = 0
pShaping = pShaping[0]
numero_amostras = 30
espaco_amostral = range(0, numero_amostras + 1)
limite_y = (-2, 2)
limite_x = (0, numero_amostras)


def sinal_analogico():
    return np.random.randn(numero_amostras + 1)


fig, ax = plt.subplots(3)

fig.set_tight_layout(True)

# Define limites
ax[0].set_ylim(limite_y)
ax[0].set_xlim(limite_x)
ax[1].set_ylim(limite_y)
ax[1].set_xlim(limite_x)
ax[2].set_ylim(limite_y)
ax[2].set_xlim((0, 30))

# Define as labels
ax[0].set_ylabel("Magnitude")
ax[0].set_xlabel("Tempo")
ax[1].set_ylabel("Magnitude")
ax[1].set_xlabel("Tempo")
ax[2].set_ylabel("Magnitude")
ax[2].set_xlabel("Tempo")

# Define o titulo das tabelas
ax[0].set_title("Sinal Analógico")
ax[1].set_title("Bits Enviados")
ax[2].set_title("Sinal Digital")

# Ativa o grid
ax[0].grid(True)
ax[1].grid(True)
ax[2].grid(True)

(line_analogico,) = ax[0].plot(sinal_analogico(), "red")
(line_enviado,) = ax[2].plot(signal.square(
    sinal_analogico()), drawstyle='steps-mid')


def animate(i):

    sinal_atual = sinal_analogico()

    # Sinal Analógico

    x_analogico = espaco_amostral
    y_analogico = sinal_atual
    line_analogico.set_data(x_analogico, y_analogico)

    # Sinal Digital

    x_digital = espaco_amostral
    y_digital = (signal.square(sinal_atual) + 1) / 2

    markerline, stemlines, baseline = ax[1].stem(
        x_digital, y_digital, use_line_collection=True)

    # Sinal Enviado

    x_enviado = np.arange(0, (numero_amostras + 1), 0.5)
    y_auxiliar = signal.square(sinal_atual)
    y_enviado = np.zeros(2*len(y_auxiliar),)
    y_enviado[::2] = y_auxiliar

    line_enviado.set_data(x_enviado, y_enviado)

    return (line_analogico, markerline, stemlines, baseline, line_enviado)


anim = FuncAnimation(fig, animate, frames=range(
    10, 110, 10), interval=1000, blit=True)
plt.grid(True)
plt.show()
