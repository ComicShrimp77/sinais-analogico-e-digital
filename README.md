# Sinais analógico e digital

Criação de um gráfico animado em Python para comparação entre sinais digitais e analógicos.

## Pacotes Necessários

- Python 3.^
- Pip
- Pipenv
  - `sudo pip install pipenv` ou `sudo pip3 install pipenv`
- python3-tk
  - Arch: `sudo pacman -S tk`
  - Ubuntu: `sudo apt-get install python3-tk`